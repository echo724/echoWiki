+++
title = "Core"
path = "Core"
date = 2023-04-14T17:47:24+09:00

[taxonomies]
tags = ["Spring"]
+++


종속 객체를 생성 및 조립해주는 도구

가장 중요한 것은 DI와 IoC(제어의 역전) 컨테이너이다.