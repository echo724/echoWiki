+++
title = "IoC 컨테이너"
path = "IoC 컨테이너"
date = 2023-04-14T17:45:37+09:00

[taxonomies]
tags = ["Spring"]
+++


# 개념
부품을 생성하고 조립하는 역할
주문서(XML/Annotation)를 통해서 가능하다.
주문서 대로 생성한 부품이 담긴 그릇
[DI](DI.md) 컨테이너라고도 함

## 왜 IoC라고 불릴까?
조립 과정이 일반적인 조립과정과 반대로 되어있어서
작은 부품 → 큰 부품

# 문제
## 스프링 컨테이너의 라이프사이클은 어떻게 되는가?
### 1. Configuration Loading
Spring 컨테이너는 설정 파일(Java based config, XML, Annotation based config)을 불러와 빈 생성 및 연결(wire)하는 방법을 이해
### 2. Bean instatiation
설정에 따라 빈([Bean](Bean.md))을 생성한다. 빈은 싱글톤(per container)이 될 도 있고 프로토타입(a new instance for each request)이 될 수도 있다.
### 3. Dependency Injection
### 4. Initialization
### 5. Bean usage
### 6. Destruction