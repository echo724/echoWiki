+++
title = "MVC View"
path = "MVC View"
date = 2023-04-25T11:54:09+09:00
+++
# Spring MVC 어디까지 왔나?
![](1b84b770c8598c2ee24e66792a99d0fb.png)

초록색 부분에서 충분히 삽질하면 좋음

# Spring MVC with template engine
![](7d802503bda25bfec02344f0c63b19c0.png)

![](b402d53f3842db4fdfde96ec63c101ac.png)
View는 Dispatcher Servlet이 처리해서 페이지를 만들고 응답

`ThymeleafView`
![](0a49fb86d24af5ed0ca6ea832e5b9b95.png)
render → html 파일 → 응답

`sample.html`
![](0bc58338c7f9586e53c663555e0961f1.png)

렌더 결과의 html을 Controller에서 반환

> 즉 View의 역할은 템플릿 엔진을 이용해 렌더한 결과를 반환하는 역할  