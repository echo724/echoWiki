+++
title = "Spring JDBC Batch Insert 사용하기"
path = "Spring JDBC Batch Insert 사용하기"
date = 2023-04-23T21:09:35+09:00

[taxonomies]
tags = ["Spring","Debug"]
+++


# Problem
Database에다가 한 게임 ID에 여러 엔티티를 저장하려고 하려는 상황.
이때 for문을 돌면서 엔티티 하나씩 insert하는 경우 DB의 접근 비용이 비싸 batct insert를 사용하도록 추천받았다.

# Reason


# Solution