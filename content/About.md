+++
title = "About"
path = "About"
date = 2023-04-28T11:06:48+09:00
+++

# echoWiki About

echo's wiki is a wiki for echo's study.

## echoWiki Structure

echoWiki has sections and pages.

### Sections
All sections are named in lowercase and separated by a slash.

ex) `wtc/debug/`

### Pages
All pages are named in initial capital letter with post date and tags.

ex) `About::2023-04-28::#About`