+++
title = "Spring Boot Actuator를 이용한 서버 Health Checking"
path = "Spring Boot Actuator를 이용한 서버 Health Checking"
date = 2023-06-07T17:37:51+09:00
[taxonomies]
tags = ["Spring"]
+++
# Spring Boot Actuator를 이용한 서버 Health Checking

“서버가 잘 돌아간다”를 어떻게 확인할 수 있을까?
서버에 접속해서 log 확인하기? 실제 웹사이트 접속해보기?
만약 서버의 DB 접속이 안좋은지를 확인하려면?

이런 서버의 여러 상태를 확인하기 위해 다양한 방법이 있을 수 있지만, Spring Actuator를 사용해 Health Checking이 가능하다.

> Spring Boot 3.x 기준으로 작성해보려고 한다.
>

# Health Checking이란?

말 그대로 서버가 정상적으로 서비스가 가능한지 확인하는 것이다.

여기서 Health란 서버 그 자체의 실행 여부 일 수 도 있고, 서버에 포함된 다양한 기능들의 작동 여부를 포함하고 있다.

# Spring Boot Actuator란?

Spring Boot에서 제공하는 production-ready 기능들을 제공하는 모듈, 프로덕션 환경에서 돌아가는 애플리케이션을 모니터링하고 관리하는 기능을 제공한다.

> 💡 **Actuator란?**
Actuator는 무언가를 움직이거나 제어하기 위한 기계 장치를 지칭하는 제조 용어이다. Actuator는 작은 변화로 많은 양의 움직임을 생성할 수 있다.

아래 방식으로 `org.springframework.boot:spring-boot-starter-actuator` 의존성을 추가시키면 사용 가능할 수 있다.

```jsx
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-actuator'
}
```

## EndPoint 설정

`application.properties`에서 아래와 같이 default는 false로 해두어 모든 기본 기능을 false로 해두고, 원하는 기능만 `management.endpoint.{기능}.enabled=true`를 설정해두어 다른 기능이 노출되는 것을 방지한다.

```jsx
management.endpoints.enabled-by-default=false
management.endpoint.health.enabled=true
```

그리고 **Endpoints**를 노출시켜줘야 제대로 열린다.

```jsx
management.endpoints.web.exposure.include=health
```

각 endpoint는 enabled되고 exposed 되어야 사용 가능하다.

info, loggers, metrics, mappings, health 등 여러 endpoint들과 기능들이 있는데 이 중에서 health 기능을 알아보려고 한다.

## Actuator의 Health Check

위의 설정대로 입력해주면, `/actuator/health`로 접근하면 아래와 같은 응답이 되돌아온다.
```
{
    "status": "UP"
}
```
정상적으로 동작하는 서버에서는 `UP`이라는 값으로 반환값이 온다.

여기서 이런 의문이 들 수 있다.
"아니 어차피 서버가 다운되서 접속이 안되면 제대로 동작 안하는거 아냐?"

하지만 서버가 잘 동작하지 않는다의 기준은 단순히 서버에 접속이 안된다 뿐만 아니라 여러가지 요인에 의해 결정되는 것이다.

예를 들어 서버의 데이터베이스와 잘 연결되지 않는다면 그것 또한 서버가 정상 작동 안하는 경우일 것이다.

**Actuator**에서는 실제로 내부의 여러 HealthContributor를 순회하면서 건강 점검을 하고, 각 상태를 수집해 하나의 Status로 반환한다.

내부의 서비스들의 상태를 보고 싶다면 아래와 같이 `health.show-details` 기능을 활성화시켜야한다.
```
management.endpoint.health.show-details=always
```

그러면 다시 `/actuator/health`를 입력할 경우 아래와 같은 응답이 온다.
```
{
    "status": "UP",
    "components": {
        "db": {
            "status": "UP",
            "details": {
                "database": "MySQL",
                "validationQuery": "isValid()"
            }
        },
        "diskSpace": {
            "status": "UP",
            "details": {
                "total": 8141348864,
                "free": 4841697280,
                "threshold": 10485760,
                "exists": true
            }
        },
        "ping": {
            "status": "UP"
        }
    }
}
```

여기서 각 **HealthIndicator**들의 status가 모두 `UP`이여야 최종 status가 `UP`으로 표시된다.

이때 하나의 상태라도 `DOWN`이 있다면 전체 서비스의 상태를 `DOWN`으로 생각하기 때문에 정말 중요한 것들만 건강 점검 기능을 활성화해야한다.

# 주의할 점
Spring Boot Actuator는 단순히 건강 검진 외에도 정말 여러 기능들을 제공하기 때문에 부주의하게 endpoint를 열어서는 안된다.

여러 사이트를 돌아다니며 /actuator가 열려있는지 확인하는 공격자들도 있다고 한다.

따라서 `actuator.base-path`도 기본 `actuator/`말고 다른 것으로 바꿔주는 것이 좋다.
```
management.endpoints.web.base-path = {{원하는 경로}}
```

이 외에도 주의점들이 몇 가지 더 있는데 필요에 따라 [확인](https://techblog.woowahan.com/9232/)해보길 바란다.

# 마무리
좀 더 원활한 협업을 하기 위해 어떤 기능을 배우면 좋을까 하는 생각에 Actuator를 학습했다. 물론 지금 레벨에서는 전혀 사용 안해도 될 기능이지만, 주요 기업들에서도 사용하는 것을 보면 자주 사용되는 기술이니 키워드만 알아둬도 좋을 것 같다.

# References

[https://toss.tech/article/how-to-work-health-check-in-spring-boot-actuator](https://toss.tech/article/how-to-work-health-check-in-spring-boot-actuator)
[https://techblog.woowahan.com/9232/](https://techblog.woowahan.com/9232/)