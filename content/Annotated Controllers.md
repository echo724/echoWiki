---
title : "Annotated Controllers"
date : 2023-04-27T19:05:41+09:00
draft : true
taxamonies:
  tags:
    - Srping
---
# `@RestController`와 `@Controller`의 차이

Spring MVC에는 2가지 **Controller Annotation**이 있다.
`@RestController`와 `@Controller`이다.

@Controller는 오래전부터 Spring에 포함되어 왔다고 한다.
> @Controller는 Spring2.5에서 등장했으며, @RequestMapping, @RequestParam, @ModelAttribute 등을 같이 공개했다.