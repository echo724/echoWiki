+++
title = "Exception Handler"
path = "Exception Handler"
date = 2023-04-20T15:22:33+09:00

[taxonomies]
tags = ["Spring"]
+++


# 개념
Spring의 Controller에서 예외를 처리하기 위한 메서드에 붙일 수 있는 어노테이션
```java
@Controller
public class SimpleController {

    // ...

    @ExceptionHandler
    public ResponseEntity<String> handle(IOException ex) {
        // ...
    }
}
```
- 위의 코드에서 IOException은 최상위 레벨로 전파된 IOException이나 IOException을 감싸고 있는 예외에 매칭된다.

함수 인자로 매칭되는 예외를 인수로 넣는다.
하지만, 여러 메서드가 매칭될 경우, 최상위 예외가 매칭된다.

`ExceptionDepthComparator`가 예외들의 depth에 기반해 정렬을 한다.

좀더 구체적으로 예외를 처리하기 위해, 다음과 같이 어노테이션에 예외 클래스를 지정할 수 있다.

```java
@ExceptionHandler({FileSystemException.class, RemoteException.class})
public ResponseEntity<String> handle(IOException ex) {
    // ...
}
```