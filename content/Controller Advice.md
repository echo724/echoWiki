+++
title = "Controller Advice"
path = "Controller Advice"
date = 2023-04-20T13:47:10+09:00

[taxonomies]
tags = ["Spring"]
+++


# 개념
`@ControllerAdvice`는 애플리케이션 전체에서 여러 controller의 일관된 행동과 처리를 적용하기 위한 설정에 붙는 어노테이션이다.

`@ExceptionHandler`, `@InitBinder`, 그리고 `@ModelAttribute`는 위의 언노테이션이 붙은  `@Controller` 클래스에만 적용된다. 하지만 `@ControllerAdvice`, `@RestControllerAdvice`에 선언된다면, 어떤 컨트롤러에도 적용될 수 있다.

`@ControllerAdvice`는 `@Component`를 달고 있는 어노테이션이기 때문에 Bean으로 등록될 수 있다.

프로그램이 시작하면 **RequestMappingHandlerMapping**과 **ExceptionHandlerExceptionResolver**이 Controller Advice 빈을 찾고, 런타임에 적용한다.

다만, Controller Advice에 붙은 [Exception Handler](Exception%20Handler.md)는 Local Controller에 붙은 Exception Handler보다 우선순위가 낮다.


# 답해보기

## ControllerAdvice를 사용할 수 있는 사례는 무엇이 있는가? 미션을 진행하는 과정에서 어디에 사용했는가?
`@ExceptionHandler`, `@InitBinder`, 그리고 `@ModelAttribute` 어노테이션들을 사용하는 경우, 모든 컨트롤러에서 중복되게 작성해야되는 경우 사용해볼 수 있을 것 같다. 추가적으로 `@ExceptionHandler`의 경우, `controller`로부터 예외 처리 코드를 분리하여 작성할 수 있다.