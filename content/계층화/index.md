+++
title = "계층화"
path = "계층화"
date = 2023-04-21T10:45:30+09:00
+++
# 웹 자동차 계층
가장 기본 구조
![](98d27957b4da8baff53f115296e3fcc8.png)

중복코드?
![](1b96e7bc0547621a2fc459a1eb4fea6a.png)

콘솔 어플리케이션을 지원하기 위해 추상화했을 것임
![](f023e416e587ab6e711bad5ebf3ec65a.png)
계층을 묶어보면 이런 모양
![](5d329c6b253e324fbb345f7642c28a8c.png)

## 계층화를 하는 이유?
Presentation은 웹이나 콘솔 아무거나 가능
데이터 접근도 여러가지 가 가능하다
변하지 않는 도메인을 보호하기 위해, 책임을 나누기 위해 계층화를 한다.

![](b40ab78c74c4e32657cd0f390402beb9.png)
![](d538a07c9fc03a2d4c3a2419b2798c7e.png)

이건 계층화? No
![](67e46c6238f46b57e0aa810e3f278f71.png)

어떤 기준으로 쓸 것인가? 
결국 계층형 구조는 유지 보수를 위함이다.

# 콘솔에서는 검증 입력값
## 검증해야할 값
![](0ead88c7dde202abb2b622cd59e86536.png)
## → 어떤 계층에서 검증을 해야할까?
> 콘솔과 웹 두가지 프레젠테이션 레이어가 있는 경우?는 도메인에서 검증하는게 좋겠구나.

## 스프링에서 제공하는 Validation
![](3509d4a035191a6e0f2b8b4a9dd9f116.png)

여담
[검증](https://stackoverflow.com/questions/20091826/should-dto-and-entity-both-have-input-validations)
