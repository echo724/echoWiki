+++
title = "DI"
path = "DI"
date = 2023-04-14T17:05:47+09:00

[taxonomies]
tags = ["Spring"]
+++
# Dependency Injection
종속성 주입(부품 조립)

다른 객체를 사용할 경우 2가지가 있음
1. Composition
```java
Class A{
	private B b;
	public A(){
		b = new B();
	}
}
```
2. Association
```java
Class A{
	private B b;
	
	//생성자 의존 주입
	public A(B b){
		this.b = b
	}
	//Setter 의존 주입
	public void setB(B b){
		this.b = b
	}
}
```

## 왜 중요한가?
만약 조합을 사용할 경우, 내부의 부품을 주입해줄 수 없음
DI를 통해 A의 입장에서는 B의 부품을 받을 수 있도록 할 수 있음
→ 부품을 갈아끼거나 업데이트 하는 경우 느슨한 결합이 선호되기에 부품 조립사용
직접 부품을 조립해서 넣어줘야하기 때문에 불편 => 이 부분은 스프링이 해줄 수 있음([IoC 컨테이너](IoC%20컨테이너.md))

## 방법
- 세터 주입
- 생성자 주입

## 조립하기
조립해주는 역할(DI)은 **스프링**에서 해줌

# 답해보기
## 의존성을 주입하는 방법에는 무엇이 있는가? 각 방법의 장단점은 무엇인가? 어떤 기준으로 나눠서 사용하는가?
- Setter Injection
- Construction Injection
- Field Injection


## DI란 무엇인가? DI를 사용하면 무엇이 좋은가?
의존성 주입, 부품 조립과 유사하다.
다른 객체를 사용할때, 의존 주입을 통해 내부 객체를 주입해줄 수 있다.
의존 객체를 생성 후 조립함으로써 결합도를 낮출 수 있다.
낮은 결합도는
- 유지보수성 향상: 독립적 수정, 업데이트 교체 가능
- 테스트 용이: 테스트 구현 또는 모의 구현으로 대체 가능. 외부 종속성에 대한 걱정이 없어짐
- 모듈성 향상
- 간소화된 구성
- 코드 가독성 향상
- 병렬 개발 용이

# Reference:
https://www.youtube.com/watch?v=WjsDN_aFfyw&list=PLq8wAnVUcTFUHYMzoV2RoFoY2HDTKru3T&index=3