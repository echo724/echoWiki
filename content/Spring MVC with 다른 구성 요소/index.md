+++
title = "Spring MVC with 다른 구성 요소"
path = "Spring MVC with 다른 구성 요소"

date = 2023-05-02T11:51:27+09:00
[taxonomies]
tags = ["Spring"]
+++

# Interceptor
이렇게 인증을 하는데 중복되는 로직이 있을 경우, 중복을 줄이기 위해 Interceptor 사용 가능
![](e72e9e2bf543e99fb2a7b3a8355045bf.png)

이런 식으로 사용 가능
![](aad6e5d27ae1477ae27757f8029f568b.png)


# MethodArgumentResovler

요청의 body나 parameters를 객체로 주입시켜줌

여기서 인증 객체를 따로 만들어서 주입도 가능


# WebMvcConfigurer
![](07be23748542c3a118ba52248159ecff.png)
- addPathPatterns: 이걸 통해 파라미터의 경로로 들어온 요청을 미리 처리할 수 있음

## addViewController(ViewControllerRegistry)

**ViewControllerRegistry**의 `addViewController`를 통해 컨트롤러 설정 없이 특정 View를 반환하도록 할 수 있다.

## addInterceptors(InterceptorRegistry)

**InterceptorRegistry**의 `addInterceptor`메서드에 Incerceptor 구현체를 넣고, `addPathPatterns` 에 패스 패턴을 넣음으로써 인터셉터 추가 가능

## addArgumentResolvers(final List\<HandlerMethodArgumentResolver\>)

HandlerMethodArgumentResolver의 리스트에 새로 **HandlerMethodArgumentResolver**를 구현한 구현체를 추가함으로써 사용 가능하다.

예를 들어 아래와 같은 HandlerMethodArgumentResolver 구현체를 작성했을때
```java
public class AuthenticationPrincipalArgumentResolver implements HandlerMethodArgumentResolver {  
	@Override  
	public boolean supportsParameter(MethodParameter parameter) {  
	return parameter.hasParameterAnnotation(AuthenticationPrincipal.class);  
	}  
	  
	@Override  
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {  
	return new LoginMember(1L, "email", 120);  
	}  
}
```

아래와 같이 추가하면 된다.
```java
@Configuration  
public class WebMvcConfiguration implements WebMvcConfigurer {
	@Override  
	public void addArgumentResolvers(final List<HandlerMethodArgumentResolver> resolvers) {  
	resolvers.add(new AuthenticationPrincipalArgumentResolver());  
	}
}
```

그리고 컨트롤러에서는 파라미터에 어노테이션을 붙임으로써 사용 가능하다.

```java
@GetMapping("/favorites")  
public ResponseEntity<List<FavoriteResponse>> showFavorites(@AuthenticationPrincipal LoginMember loginMember) { ... }
```
