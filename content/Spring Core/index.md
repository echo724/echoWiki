+++
title = "Spring Core"
path = "Spring Core"
date = 2023-04-18T10:44:27+09:00
+++

# 의존 방식에 따른 결합도
## 의존성?
![](d0f0a69100d1028ad1918e9577e23317.png)

의존성: 변경이 있을때 변경이 될 여지가 있음
 
> 의존도가 높으면 확장성/유지보수성이 떨어짐

Hello This is how

## 의존하는 객체 생성/사용
```java
class A{
	priavte ResultDAO resultDAO;

	A(){
		this.resultDAO = new ResultDAO();
	}

	call)()
}
```
변화에 의한 영향을 크게 받게 된다.

## 의존 관계 줄이는 법?
### 1. 추상화(인터페이스) - 사용 의존성 줄이기
![](6ab2af6a35b2f2c70f44d674c9016b24.png)

→ 사용에 대한 의존성이 줄어듬
### 2. 팩토리 - 생성 의존성 줄이기
![](2024bb7f350cd23a8ce7087eede18009.png)
### 3. 의존성 삽입 - 생성 의존성 줄이기
![](28b8babd289a0bd8ccc3c2ad3e677558.png)
조립기가 의존성을 삽입 해줄 수 있음
> Spring이 조립기 역할을 함
조립기 - **컨테이너**(Bean 팩토리, IoC 컨테이너, Spring 컨테이너 등으로 불림)
각 객체 - **스프링 Bean**

# 컨테이너
![Screenshot 2023-04-18 at 11.03.33](629c2676025539733e1ed61541920de2.png)
모든 것을 관리해주는가? X
설정된 객체에 대해서만 관리해줌

## 관리 방식
1. XML
2. ==Annotation-based configuration==
3. Java-based configuration

## Bean
- Component
	- Service
	- Controller
	- Repository

## Scan 방식
`@SpringBootApplication` 안에 `@ComponentScan` 애너테이션을 통해 등록할 빈을 스캔할 **classpath** 지정

# QnA
## Repository와 DAO 차이
- `DAO`는 일반적으로 개발자가 직접 쿼리를 작성하고 JDBC를 이용해서 데이터베이스와 상호작용합니다
- `Repository`는 주로 ORM 프레임워크와 함께 사용하여 데이터 엑세스 로직을 추상화한다고 합니다