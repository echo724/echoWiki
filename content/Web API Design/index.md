+++
title = "Web API Design"
path = "Web API Design"
date = 2023-04-28T10:41:35+09:00

[taxonomies]
tags = ["Web"]
+++

카트에 상품 추가 AP(1)
![](befd58ca12add302430f66a414a03a8b.png)
카트에 상품 추가 API(2)
![](616ece628ffef36a7ad19e77ba4a8489.png)

뭐가 올바른 설계일까??

# Web API?
![](9336c953aba2cd79f2928eabaf7232b6.png)
## Request와 Response를 잘 설계해야하는 이유?
여러명에서 일을 나눠서 하기 때문에, 약속을 잘 정해야함 → 소통이 안될 수 있다.

# 웹 API 설계란?
![](75ab6df7b87e46b59393a18031d32a46.png)

# HTTP
![](a336d893177006e8050c07ad154df3e0.png)

## RFC 2616(HTTP/1.1)
- HTTP 1.1 명세
- URI, Media Type, HTTP 형식, Method, Status 등 HTTP의 요소들에 대한 정의 및 설명

"RFC Header" 이런식으로 검색하면 Header 명세에 대해 자세히 알 수 있음

# 이미 명세가 있는데 따로 설계를 할 필요가 있는가?
![](14bc76f7f2a40c7cdbf73c421830baae.png)
HTTP는 API를 설계를 사용되는데 필요한 요소

# 좋은 API 설계란?
## 꼭 잘해야하는가?
- 그냥 서버와 클라이언트 간의 약속만 잘 지키면 되는거 아닌가?

## 잘 해야한다..
![](005b80c24498e2f00aa566c4a8fe9f94.png)
API 변경 → 클라이언트의 막대한 변경이 생길 수 있음

## 이미 대기업에서 설계하지 않았는가?
각 기업마다 다 다름

## 사실 API 설계 표준화 시도는 있었다..
![](c5f098ff008c9cb8aa44f9a204bebccc.png)

## 좋은 API 설계의 특징
![](9733cef015da50a2c3a69ac118f6a855.png)

# REST API
Representational state transfer
추상적인 스타일 → 구체화한 것이 규약

> 제약 조건을 읽어보는 것은 추천한다.

## REST 용어의 사용
![](9bfd06f4631923c7c1372e58ac40bf6d.png)

## REST 미션에 잘 사용하기
![](7f6d7912a22fd6931c4b4b7fbb5fece8.png)

# 용어 설명
## 리소스
![](2dc2ae02f9ebd44885ec23f7b7a928b8.png)
- 클라이언트에서 저 서비스가 어떤 **자원**을 제공해줄수 있는지 관점으로 보라
- 내부적으로 클래스가 어떻게 설계되어있는지와 다를 수 있음
- 동일시 생각하지 않기

## 리소스는 클라이언트가 얻기 원하는 것이다.
![](137e7c9f41afa47f7847bc4d2e5e7782.png)

## URL - Path & Query
- path: 계층적 형태로 구성된 데이터
- query: 비계층적 형태로 구성된 데이터

![](7797cb6ad248ab6bdf5362716c37dcea.png)
- 접근하고 하즌 것들은 계층이 없음

## 리소스의 표현(Representation)
![](896a742520c006af0c95dbfe8989d19b.png)
레이싱카를 받고 싶다.

HTTP로 받는 것은 레이싱 카를 받을까? no
레이싱카의 **표현**을 받는 것이다.

## 표현은 어떻게 해석할 수 있을까?
리소스의 표현만 보내주면 안되고, **어떻게 해석을 해야하는지** 보내주어야함 ~= **헤더 정보**

> Note
> Content Negotiation

# 많이 사용하는 API 가이드
## 리소스 네이밍 가이드
사용하는 것을 참고해라
![](690e16e216a23801f7c15b079ff875d4.png)

[참고자료](https://restfulapi.net/)