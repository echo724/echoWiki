+++
title = "Layered Architecture"
path = "Layered Architecture"
date = 2023-04-21T16:19:51+09:00

[taxonomies]
tags = ["Architecture"]
+++
# 개념

## 레이어드 아키텍처란?
n-계층 구조 아키텍처라고 불리고, 수평 계층으로 구성된 아키텍처 패턴이다.
계층은 구성 요소나 코드를 논리적으로 분리한 것이다.

이 프레임워크에서는,  비슷하거나 관련있는 컴포넌트들이 같은 계층에 위치한다.

## 특징
- 바로 위, 아래 레이어와만 연결된다.
- 레이어는 격리되어있다. 레이어를 수정했을때 다른 레이어에 영향을 미치지 않는다.
- 관심사 분리가 되어있다.

## 구성요소
- Presentation Layer: 소프트웨어와 사용자 상호 작용을 담당한다.
- Application/Business Layer: 기능적 요구사항 수행을 담당한다.
- Domain Layer: 알고리즘 및 프로그래밍 구성 요소를 담당한다.
- Infra/Persistence/Database Layer: 데이터베이스 처리 담당한다.

## 장점
- 의존성이 줄어든다
- 테스트가 쉬워진다.
- 유지보수가 쉽다.

## 단점
- 확장이 어렵다.
- 레이어간 상호 의존성이 있을 수 있다.
- 병렬처리가 불가능하다.

# 답해보기
## 1. Layerd Architecture에 대해 설명하시오. 

## 2. 서비스 레이어의 역할은 무엇인가? 
비지니스 로직을 처리하고 Presentation Layer와 Persistence Layer를 중개하는 역할을 한다.

# Reference
[https://www.baeldung.com/cs/layered-architecture](https://www.baeldung.com/cs/layered-architecture)