+++
title = "Test with Spring"
path = "Test with Spring"
date = 2023-04-25T11:58:06+09:00
+++
현재 상황: 스프링 활용법을 배우는 단계 → 스프링을 응용해서 잘 쓰는 방법 학습하기

어떻게 코드를 잘 짤 수 있을까? → 테스트

테스트를 통해 확인하기

어디를 테스트해야할까?
![](7304408a2599d558d90b1a954e8acb7c.png)

단위, 통합, 기능테스트에 대해서 알아볼 예정

![](19e38013a0aac2aa99dc911d628700d4.png)

웹 브라우저와 웹 애플리케이션을 테스트할 것임
![](d6c1cee202fe243320874d3ddef69ce0.png)
위에와 같은 환경을 아래와 같은 환경으로 테스트해볼 것 임
![](364a9413f0c69031e173f3974dc35414.png)

`@SpringBootTest`를 이용할 예정
# 웹 애플리케이션 테스트
## @SpringBootTest
![](595786241ecd36ce63d833c8bb26ebf5.png)
## WebEnvironment
![](44fcd0737550aa24502535db82af1cc9.png)

### RANDOM_PORT와 DEFINED_PORT
둘다 실제 웹 환경 구성
DEFINED_PORT: 실제 8080 포트
RANDOM_PORT: 랜덤 포트

### MOCK
가짜 웹 환경을 제공

## RestAssured
- 테스트에서 웹 브라우저와 같은 환경
- `given`, `when`, `then` 구문으로 테스트 가능

## 답해보기
- 왜 부분이 아닌 전체를 검증?
- 부분 vs 전체
- 무엇이 더 중요할까?