+++
title = "Story1 HTTP Request 메세지 작성"
path = "Story1 HTTP Request 메세지 작성"
date = 2023-04-17T12:11:31+09:00

[taxonomies]
tags = ["Network"]
+++


## 1. URL 입력
### URL은 다양하다
- http:
- file:
- mailto:
- ftp:

### 다양한 이유는 브라우저의 기능이 많기 때문
- 파일 다운로드
- 메일 클라이언트

### 필요에 따라 URL에 포함시키는 내용 다르게 가능
- http:나 ftp:의 경우 도메인명이나 파일 경로명 포함
- mailto:의 경우 메일 주소를 URL에 포함
- 포트 번호도 포함 가능

### URL에는 하나의 공통점
URL 맨 앞 부분 문자열 = 프로토콜 종류

## 2. URL 해독
브라우저가 제일 먼저 하는 일은 URL 해독

## 3. 파일명 생략한 경우
#### (a) http://wootechcourse.com/blog/
끝이 `/`로 끝나는 것은 `/dir/`의 다음에 써야할 파일명을 생략한다는 것
이런 경우  서버가 파일명을 미리 설정해둠
#### (b) http://wootechcourse.com/
루트 디렉토리 /가 지덩되고 파일명 생략된 것
루트에 있는 index.html 등의 파일에 엑세스
#### (c) http://wootechcourse.com
끝의 /까지 생략
지나친 생략일 수 있지만 인정되고 있음
루트 디렉토리에 미리 설정된 파일을 지정
#### (d) http://wootechcourse.com/something
맨 끝에 `/`가 없으므로 something을 **파일명**으로 보는 것이 맞을 것
하지만 가끔 디렉토리 끝에 있는 `/`까지 생략해버리는 경우가 있다.
something이라는 파일이 있다면 파일명으로
디렉토리가 있다면 디렉토리명으로

## 4. HTTP의 기본 개념
리퀘스트 메세지 안에 '무엇을', '어떻게'에 대한 내용이 쓰여져 있음
### 무엇: URI(Uniform Resource Identifier)
파일 이름, CGI 프로그램의 파일명, 혹은 http:로 시작하는 URL
